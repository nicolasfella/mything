use std::mem::MaybeUninit;
use cxx::ExternType;
use cxx::type_id;

#[cxx::bridge()]
mod ffi {
    // Rust types and signatures exposed to C++.
    extern "Rust" {

        fn do_stuff(thing: MyThing);
    }

    // C++ types and signatures exposed to Rust.
    unsafe extern "C++" {
        include!("demo/include/blobstore.h");

        type MyThing = super::MyThing;

        #[rust_name = "mything_drop"]
        fn drop(string: &mut MyThing);

        #[rust_name = "mything_init_from_mything"]
        fn construct(string: &MyThing) -> MyThing;

        #[rust_name = "mything_gibNumber"]
        fn gibNumber(thing: &MyThing) -> i32;

        fn cmain();
    }
}

#[repr(C)]
pub struct MyThing {
    // d: cxx::UniquePtr<ffi::MyThingPrivate>,
    _space: MaybeUninit<usize>
}

impl MyThing {

     pub fn gib_number(&self) -> i32 {
        ffi::mything_gibNumber(self)
    }

}

impl Drop for MyThing {
    /// Destroys the string.
    fn drop(&mut self) {
        ffi::mything_drop(self)
    }
}

impl Clone for MyThing {
    fn clone(&self) -> Self {
        ffi::mything_init_from_mything(self)
    }
}

unsafe impl ExternType for MyThing {
    type Id = type_id!("MyThing");
    type Kind = cxx::kind::Trivial;
}

pub fn do_stuff(thing: MyThing)
{
    println!("Hello from Rust. You value is {}", thing.gib_number());
}

fn main() {
    ffi::cmain();
}
