#include "demo/include/blobstore.h"
#include "demo/src/main.rs.h"
#include <iostream>

int gibNumber(const MyThing& thing) {
  return thing.gibNumber();
}

MyThing::MyThing(const MyThing &other)
{
    d = std::make_unique<MyThingPrivate>(*other.d);
}

MyThing &MyThing::operator=(const MyThing &other)
{
    MyThing temp(other);
    std::swap(*d, *temp.d);
    return *this;
}

void cmain() {
  std::cout << "Hello from C++" << std::endl;

  MyThing thing;

  std::cout << "Val from C++ " << thing.gibNumber() << std::endl;

  do_stuff(thing);
}
