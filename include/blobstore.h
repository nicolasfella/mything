#pragma once
#include "rust/cxx.h"
#include <memory>

template<typename T>
void
drop(T& value)
{
  value.~T();
}

template<typename T, typename... Args>
T
construct(Args... args)
{
  return T(args...);
}

class MyThingPrivate
{
public:
  int i = 13;
};

class MyThing {
  std::unique_ptr<MyThingPrivate> d;

public:
  MyThing() {
    d = std::make_unique<MyThingPrivate>();
  }

  MyThing(const MyThing &);
  MyThing &operator=(const MyThing &);

  int gibNumber() const {
    return d->i;
  }

};

int gibNumber(const MyThing& thing);

template<>
struct rust::IsRelocatable<MyThing> : ::std::true_type
{
};

void cmain();
